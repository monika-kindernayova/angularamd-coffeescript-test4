gulp = require 'gulp'
coffee = require 'gulp-coffee'
serve = require 'gulp-serve'
karma = require 'gulp-karma'

paths =
	coffee: [
		'src/coffee/**/*.coffee'
		'!src/coffee/**/*_test.coffee'
	]
	coffeetest: 'src/coffee/**/*_test.coffee'
	unit: 'test/unit/'
	scripts: 'src/scripts/'
	styles: 'src/styles/**/*.css'
	images: 'src/images/**'
	indexs: 'src/**/*.html'
	test: [
		'src/libs/**/*.js'

		'src/scripts/**/*.js'

		'test/**/*_test.js'

    'test/require.conf.js'
	]

gulp.task 'coffee', ->
	gulp.src paths.coffee
		.pipe (coffee bare: true)
		.pipe gulp.dest paths.scripts
	gulp.src paths.coffeetest
		.pipe (coffee bare: true)
		.pipe gulp.dest paths.unit

#测试
gulp.task 'test', -> gulp
	.src paths.test
	.pipe karma
		configFile: 'test/my.conf.js'
		action: 'watch'


gulp.task 'watch', ['coffee'], ->
	gulp.watch paths.coffee, ['coffee']

gulp.task 'serve', ['coffee', 'watch'], serve
	root: './src'
	port: 8080
	Livereload: true

gulp.task 'default', [
	'coffee'
	'serve'
	'watch'
], ->