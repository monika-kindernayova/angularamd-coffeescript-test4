define [
  "app"
  "angularAMD"
  "angular"
  "angular-ui-router"
  "angular-mocks"
], ->
  describe "home/home-controller.js", ->
    scope = undefined
    ctrl = undefined
    beforeEach inject(($rootScope, $controller) ->
      scope = $rootScope.$new()
      ctrl = $controller("HomeController",
        $scope: scope
      )
      return
    )
    it "should define more than 5 awesome things", ->
      expect(scope.message).toBeDefined()
      return

    return

  return
